﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using BartqImHere.Resources;
using System.Device.Location;
using Windows.Devices.Geolocation;
using Microsoft.Phone.Maps.Controls;
using System.Windows.Shapes;
using System.Windows.Media;
using Microsoft.Phone.Maps.Services;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.Storage.Streams;
using BartqImHere.Code;
using BartqImHere.Models;
using Microsoft.Phone.Tasks;
using System.Threading;
using RateMyApp;
using Coding4Fun.Toolkit.Controls;
using BartqGoHome.Common;

namespace BartqImHere
{
    public partial class MainPage : PhoneApplicationPage
    {
        // Constructor
        public MainPage()
        {
            InitializeComponent();

            var vm = new MainViewModel();

            InjectMapFancyTokens();

            this.DataContext = vm;

            App.MapComponent = Map;
        }

        private void InjectMapFancyTokens()
        {
            Microsoft.Phone.Maps.MapsSettings.ApplicationContext.ApplicationId = "cde33f44-006f-4bc1-b727-32fd6ea70500";
            Microsoft.Phone.Maps.MapsSettings.ApplicationContext.AuthenticationToken = "EEDTK_2tc9AEbWwhoIhTlg";
        }


        // Sample code for building a localized ApplicationBar
        //private void BuildLocalizedApplicationBar()
        //{
        //    // Set the page's ApplicationBar to a new instance of ApplicationBar.
        //    ApplicationBar = new ApplicationBar();

        //    // Create a new button and set the text value to the localized string from AppResources.
        //    ApplicationBarIconButton appBarButton = new ApplicationBarIconButton(new Uri("/Assets/AppBar/appbar.add.rest.png", UriKind.Relative));
        //    appBarButton.Text = AppResources.AppBarButtonText;
        //    ApplicationBar.Buttons.Add(appBarButton);

        //    // Create a new menu item with the localized string from AppResources.
        //    ApplicationBarMenuItem appBarMenuItem = new ApplicationBarMenuItem(AppResources.AppBarMenuItemText);
        //    ApplicationBar.MenuItems.Add(appBarMenuItem);
        //}

        private GeoCoordinate MyCoordinate = null;

        private double _accuracy = 0.0;
        bool manualMode = false;
        DateTime lastGeoAttempt = DateTime.Now.AddDays(-1);

        private Geolocator geolocator = null;

        private void RegisterGeolocator()
        {
            if (geolocator == null)
            {
                geolocator = new Geolocator();
                geolocator.DesiredAccuracy = PositionAccuracy.High;
                geolocator.ReportInterval = 100;
                geolocator.PositionChanged += geolocator_PositionChanged;
            }
        }

        private void PromptForGeoPermission(MainViewModel vm)
        {
            if (App.Settings.GeoPermission != "yes")
            {
                var messagePrompt = new MessagePrompt
                {
                    Title = "User Location Data",
                    Message = "This application uses your location. Do you wish "
                        + "to give it permission to use your location?",
                    IsAppBarVisible = false,
                    IsCancelVisible = true
                };
                messagePrompt.Completed += messagePrompt_Completed;
                messagePrompt.Show();
            }
            else RegisterGeolocator();
            vm.PermissionChecked = true;
        }

        void messagePrompt_Completed(object sender, PopUpEventArgs<string, PopUpResult> e)
        {
            if (e.PopUpResult == PopUpResult.Ok)
            {
                App.Settings.GeoPermission = "yes";
                RegisterGeolocator();
            }
            else if (e.PopUpResult == PopUpResult.Cancelled)
            {
                App.Settings.GeoPermission = "no";

            }
        }

        void geolocator_PositionChanged(Geolocator sender, PositionChangedEventArgs args)
        {
            if (args.Position != null)
            {
                _accuracy = args.Position.Coordinate.Accuracy;


                Dispatcher.BeginInvoke(() =>
                {
                    MyCoordinate = new GeoCoordinate(args.Position.Coordinate.Point.Position.Latitude, args.Position.Coordinate.Point.Position.Longitude);
                    App.MyGeoCoordinate = MyCoordinate;
                    App.MyGeoTime = DateTime.Now;

                    if (!manualMode)
                    {
                        Map.Center = MyCoordinate;
                    }
                    DrawMapMarkers();



                    var tiles = MyLiveTile.GetTiles();
                    bool tile = tiles == null || tiles.Count() == 0;

                    if ((DateTime.Now - lastGeoAttempt).TotalMinutes > 1.0
                        && !tile)
                    {
                        if (App.Settings.Notifiactions.Count == 0)
                        {
                            ThreadPool.QueueUserWorkItem((_) =>
                            {
                                Thread.Sleep(200);
                                Dispatcher.BeginInvoke(() =>
                                {
                                    DumpMapImages(Map, "temp");
                                    ChooseContacts.MoveTempFiles();
                                    MyLiveTile.UpdateLiveTile("tile");
                                });
                            });
                        }

                        ActualizeAddress();
                        lastGeoAttempt = DateTime.Now;
                    }
                    else if (!tile)
                    {
                        MyLiveTile.UpdateLiveTile("tile");
                    }
                });

            }
        }

        private void ActualizeAddress()
        {
            GeoCoordinate geoCoordinate = MyCoordinate;
            MyReverseGeocodeQuery = new ReverseGeocodeQuery();
            MyReverseGeocodeQuery.GeoCoordinate = new GeoCoordinate(geoCoordinate.Latitude, geoCoordinate.Longitude);
            MyReverseGeocodeQuery.QueryCompleted += ReverseGeocodeQuery_QueryCompleted;
            MyReverseGeocodeQuery.QueryAsync();
        }

        private void DrawAccuracyRadius(MapLayer mapLayer)
        {
            // The ground resolution (in meters per pixel) varies depending on the level of detail
            // and the latitude at which it’s measured. It can be calculated as follows:
            double metersPerPixels = (Math.Cos(MyCoordinate.Latitude * Math.PI / 180) * 2 * Math.PI * 6378137) / (256 * Math.Pow(2, Map.ZoomLevel));
            double radius = _accuracy / metersPerPixels;

            Ellipse ellipse = new Ellipse();
            ellipse.Width = radius * 2;
            ellipse.Height = radius * 2;
            ellipse.Fill = new SolidColorBrush(Colors.Green);
            ellipse.Opacity = 0.5;
            //ellipse.Tap += GeocodeMarker_Click;

            MapOverlay overlay = new MapOverlay();
            overlay.Content = ellipse;
            overlay.GeoCoordinate = new GeoCoordinate(MyCoordinate.Latitude, MyCoordinate.Longitude);
            overlay.PositionOrigin = new Point(0.5, 0.5);
            mapLayer.Add(overlay);
        }


        private ReverseGeocodeQuery MyReverseGeocodeQuery = null;


        private void DrawMapMarkers()
        {
            Map.Layers.Clear();
            MapLayer mapLayer = new MapLayer();

            // Draw marker for current position
            if (MyCoordinate != null)
            {
                DrawAccuracyRadius(mapLayer);
                DrawMapMarker(MyCoordinate, Colors.Green, mapLayer);
            }



            Map.Layers.Add(mapLayer);
        }

        private void DrawMapMarker(GeoCoordinate coordinate, Color color, MapLayer mapLayer)
        {
            // Create a map marker
            Ellipse tag = new Ellipse();
            tag.Width = 10;
            tag.Height = 10;
            tag.Fill = new SolidColorBrush(color);

            // Enable marker to be tapped for location information
            tag.Tag = new GeoCoordinate(coordinate.Latitude, coordinate.Longitude);
            //tag.Tap += GeocodeMarker_Click;

            // Create a MapOverlay and add marker
            MapOverlay overlay = new MapOverlay();
            overlay.Content = tag;
            overlay.GeoCoordinate = new GeoCoordinate(coordinate.Latitude, coordinate.Longitude);
            overlay.PositionOrigin = new Point(0.5, 0.5);
            mapLayer.Add(overlay);
        }

        private void ReverseGeocodeQuery_QueryCompleted(object sender, QueryCompletedEventArgs<IList<MapLocation>> e)
        {
            if (e.Error == null)
            {
                if (e.Result.Count > 0)
                {
                    MapAddress address = e.Result[0].Information.Address;
                    App.MyAddress = address;
                    var vm = (MainViewModel)DataContext;
                    string str = FormatAddress(address);

                    vm.Address = str;
                }

            }
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {

            var vm = new MainViewModel();

            if (!vm.PermissionChecked)
            {
                PromptForGeoPermission(vm);
            }
            else
                if (App.Settings.GeoPermission == "yes")
                {
                    RegisterGeolocator();
                }
                else
                    UnregisterGeolocator();

            var tiles = MyLiveTile.GetTiles();

            base.OnNavigatedTo(e);
        }

        public static string FormatAddress(MapAddress address, string separator = " ")
        {
            string str = string.Empty;
            string cty = str;
            string cnt = str;

            if (!string.IsNullOrWhiteSpace(address.HouseNumber))
                str += address.HouseNumber;

            if (!string.IsNullOrWhiteSpace(address.Street))
                str += ((str.Length == 0) ? string.Empty : " ") + address.Street;

            if (!string.IsNullOrWhiteSpace(address.City))
                cty += address.City;

            if (!string.IsNullOrWhiteSpace(address.PostalCode))
                cty += ((cty.Length == 0) ? string.Empty : " ") + address.PostalCode;

            if (!string.IsNullOrWhiteSpace(address.StateCode))
                cnt += address.StateCode;

            if (!string.IsNullOrWhiteSpace(address.Country))
                cnt += ((cnt.Length == 0) ? string.Empty : " ") + address.Country;

            if (!string.IsNullOrWhiteSpace(cty))
                str += "," + separator + cty;

            if (!string.IsNullOrWhiteSpace(cnt))
                str += "," + separator + cnt;
            return str;
        }

        private void ApplicationBarIconButton_Click(object sender, EventArgs e)
        {
            manualMode = false;
            if (MyCoordinate != null)
                Map.Center = MyCoordinate;
            Map.ZoomLevel = 16;
        }

        private void Map_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            manualMode = true;
        }

        private void Map_ZoomLevelChanged(object sender, MapZoomLevelChangedEventArgs e)
        {
            DrawMapMarkers();
            var vm = (MainViewModel)DataContext;
            vm.Zoom = Map.ZoomLevel;
        }

        private void PhoneApplicationPage_Loaded(object sender, RoutedEventArgs e)
        {
            LittleWatson.CheckForPreviousException();
        }

        private void ApplicationBarIconButton_Click_1(object sender, EventArgs e)
        {
            DumpMapImages(Map, "temp");
            NavigationService.Navigate(new Uri("/ChooseContacts.xaml?kind=Email", UriKind.Relative));
            /*var emailAddressChooserTask = new EmailAddressChooserTask();
            emailAddressChooserTask.Completed += emailAddressChooserTask_Completed;
            emailAddressChooserTask.Show();*/
        }

        void emailAddressChooserTask_Completed(object sender, EmailResult e)
        {

            /*if (e.TaskResult == TaskResult.OK)
            {
                MyLiveTile.UpdateLiveTile(12, this.Map);
                EmailComposeTask emailComposeTask = new EmailComposeTask();
                
                emailComposeTask.Subject = "message subject";
                emailComposeTask.Body = "message body";
                emailComposeTask.To = "recipient@example.com";
                emailComposeTask.Cc = "cc@example.com";
                emailComposeTask.Bcc = "bcc@example.com";

                emailComposeTask.Show();
            }*/
        }

        private void ApplicationBarIconButton_Click_2(object sender, EventArgs e)
        {
            DumpMapImages(Map, "temp");
            NavigationService.Navigate(new Uri("/ChooseContacts.xaml?kind=Text", UriKind.Relative));
        }

        private void DumpMapImages(UIElement Map, string filename)
        {
            //Medium size Tile 336x336 px
            //Crete image for BackBackgroundImage in IsoStore

            MyLiveTile.RenderBackground(Map, 336, 336, filename + ".jpg");

            //Crete image for WideBackBackgroundImage in IsoStore
            MyLiveTile.RenderBackground(Map, 691, 336, "wide" + filename + ".jpg");
        }

        private void ApplicationBarMenuItem_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/About.xaml", UriKind.Relative));
        }

        private void UnregisterGeolocator()
        {
            if (geolocator != null)
            {
                geolocator.PositionChanged -= geolocator_PositionChanged;
                geolocator.ReportInterval = uint.MaxValue;
                geolocator = null;
            }
        }

        private void ApplicationBarMenuItem_Click_1(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/Settings.xaml", UriKind.Relative));
        }

        private void ApplicationBarMenuItem_Click_2(object sender, EventArgs e)
        {
            DumpMapImages(Map, "temp");
            ChooseContacts.MoveTempFiles();
            MyLiveTile.CreateLiveTile("tile");
        }

        private void HeadingSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (Map != null)
                Map.Heading = e.NewValue;
        }

        private void PitchSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (Map != null)
                Map.Pitch = e.NewValue;
        }

        private void ZoomSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (Map != null)
                Map.ZoomLevel = e.NewValue;
        }

        private void Map_PitchChanged(object sender, MapPitchChangedEventArgs e)
        {
            var vm = (MainViewModel)DataContext;
            vm.Pitch = Map.Pitch;
            DrawMapMarkers();
        }

        private void Map_HeadingChanged(object sender, MapHeadingChangedEventArgs e)
        {
            var vm = (MainViewModel)DataContext;
            vm.Heading = Map.Heading;
            DrawMapMarkers();
        }

        private void ApplicationBarMenuItem_Click_3(object sender, EventArgs e)
        {
            App.VMB.DisplayAd("1383aef7", LayoutRoot);
        }
    }
}