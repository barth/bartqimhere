﻿using Microsoft.Phone.Shell;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace BartqImHere.Code
{
    public class MyLiveTile
    {

        public static void UpdateLiveTile(string filename)
        {
            var tiles = GetTiles();
            if (tiles != null)
            foreach(var tile in tiles)
            {
                int cnt = App.Settings.Notifiactions.Count;
                FlipTileData flipTile = CreateFlipTileData(cnt, filename);

                //Update Live Tile
                tile.Update(flipTile);
            } else {
                // once it is created flip tile
                //CreateLiveTile(filename);
            }
        }

        public static IEnumerable<ShellTile> GetTiles()
        {
            return ShellTile.ActiveTiles.ToList();
        }

        public static void CreateLiveTile(string filename)
        {
            Uri tileUri = new Uri("/MainPage.xaml?tile=flip", UriKind.Relative);
            ShellTileData tileData = CreateFlipTileData(1, filename);
            ShellTile.Create(tileUri, tileData, true);
        }

        private static FlipTileData CreateFlipTileData(int? count, string filename)
        {
            FlipTileData flipTile = new FlipTileData();
            flipTile.Title = "Bartq ImHere";
            flipTile.BackTitle = "Bartq ImHere";
            flipTile.Count = count;

            flipTile.BackContent = " ";
            flipTile.WideBackContent = " ";

            //Medium size Tile 336x336 px            
            //Crete image for BackBackgroundImage in IsoStore

            flipTile.BackgroundImage = new Uri("/Assets/Tiles/FlipCycleTileMedium.png", UriKind.Relative); //Default image for Background Image Medium Tile 336x336 px
            //End Medium size Tile 336x336 px

            //Wide size Tile 691x336 px
            flipTile.WideBackgroundImage = new Uri("/Assets/Tiles/FlipCycleTileLarge.png", UriKind.Relative); ////Default image for Background Image Wide Tile 691x336 px

            using (IsolatedStorageFile isf = IsolatedStorageFile.GetUserStoreForApplication())
            {
                if(isf.FileExists("/Shared/ShellContent/" + filename + ".jpg"))
                    flipTile.BackBackgroundImage = new Uri(@"isostore:/Shared/ShellContent/" + filename + ".jpg", 
                        UriKind.Absolute); //Generated image for Back Background 336x336
               
                //Crete image for WideBackBackgroundImage in IsoStore
                if (isf.FileExists("/Shared/ShellContent/wide" + filename + ".jpg"))
                    flipTile.WideBackBackgroundImage = new Uri(@"isostore:/Shared/ShellContent/wide" + filename + ".jpg", 
                        UriKind.Absolute);
            }
            //End Wide size Tile 691x336 px
            return flipTile;
        }

        private static void RenderText(string text, int width, int height, int fontsize, string imagename)
        {
            WriteableBitmap b = new WriteableBitmap(width, height);

            var canvas = new Grid();
            canvas.Width = b.PixelWidth;
            canvas.Height = b.PixelHeight;

            var background = new Canvas();
            background.Height = b.PixelHeight;
            background.Width = b.PixelWidth;

            //Created background color as Accent color
            SolidColorBrush backColor = new SolidColorBrush((Color)Application.Current.Resources["PhoneAccentColor"]);
            background.Background = backColor;

            var textBlock = new TextBlock();
            textBlock.Text = text;
            textBlock.FontWeight = FontWeights.Bold;
            textBlock.TextAlignment = TextAlignment.Left;
            textBlock.HorizontalAlignment = HorizontalAlignment.Center;
            textBlock.VerticalAlignment = VerticalAlignment.Stretch;
            textBlock.Margin = new Thickness(35);
            textBlock.Width = b.PixelWidth - textBlock.Margin.Left * 2;
            textBlock.TextWrapping = TextWrapping.Wrap;
            textBlock.Foreground = new SolidColorBrush(Colors.White); //color of the text on the Tile
            textBlock.FontSize = fontsize;

            canvas.Children.Add(textBlock);

            b.Render(background, null);
            b.Render(canvas, null);
            b.Invalidate(); //Draw bitmap

            //Save bitmap as jpeg file in Isolated Storage
            using (IsolatedStorageFile isf = IsolatedStorageFile.GetUserStoreForApplication())
            {
                using (IsolatedStorageFileStream imageStream = new IsolatedStorageFileStream(imagename, System.IO.FileMode.Create, isf))
                {
                    b.SaveJpeg(imageStream, b.PixelWidth, b.PixelHeight, 0, 100);
                }
            }
        }

        public static void RenderBackground(UIElement backgroundElement, int width, int height, string imagename)
        {
            try
            {
                var size = backgroundElement.RenderSize;
                int cropx = (int)((size.Width - width) / 2);
                int cropy = (int)((size.Height - height) / 2);
                WriteableBitmap b = new WriteableBitmap(backgroundElement, new TranslateTransform());
                b.Invalidate();
                b = b.Crop(cropx, cropy, width, height);


                //Save bitmap as jpeg file in Isolated Storage
                using (IsolatedStorageFile isf = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    using (IsolatedStorageFileStream imageStream = new IsolatedStorageFileStream(imagename, System.IO.FileMode.Create, isf))
                    {
                        b.SaveJpeg(imageStream, b.PixelWidth, b.PixelHeight, 0, 100);
                    }
                }
            }
            catch {
                if (Debugger.IsAttached)
                {
                    Debugger.Break();
                }
            }
        }
    }
}
