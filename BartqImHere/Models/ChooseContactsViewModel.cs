﻿using BartqGoHome.ViewModels;
using BartqImHere.Code;
using Microsoft.Phone.UserData;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using Windows.System.Threading;

namespace BartqImHere.Models
{
    public class ChooseContactsViewModel : BaseViewModel
    {
        public enum Kind { Email, Text };
        
        public ObservableCollection<ContactEx> ContactsList { get; internal set; }

        public Kind Kindd
        {
            get;
            internal set;
        }

        public string GoButtonText
        {
            get
            {
                if (Kindd == Kind.Email)
                    return "Send email";
                else return "Send text";
            }
        }

        public Visibility EmptyList
        {
            get
            {
                return (ContactsList.Count == 0 && !evaluating) ? Visibility.Visible : Visibility.Collapsed;
            }
        }

        public Visibility ProgressBarVisible
        {
            get
            {
                return evaluating ? Visibility.Visible : Visibility.Collapsed;
            }
        }

        public bool IsSendEnabled
        {
            get
            {
                return App.MyGeoCoordinate != null;
            }
        }

        protected bool evaluating=true;
        protected Dispatcher dispatcher;

        public ChooseContactsViewModel(Kind kind, Dispatcher dispatcher)
        {
            this.Kindd = kind;
            this.dispatcher = dispatcher;
            ContactsList = new ObservableCollection<ContactEx>();
        }

        public async Task Fill()
        {
            evaluating = true;
            await ThreadPool.RunAsync((_) =>
            {
                Contacts cons = new Contacts();

                //Identify the method that runs after the asynchronous search completes.
                cons.SearchCompleted += cons_SearchCompleted;

                //Start the asynchronous search.
                cons.SearchAsync(String.Empty, FilterKind.None, "Abracadabra get me those!");
            });
        }

        void cons_SearchCompleted(object sender, ContactsSearchEventArgs e)
        {
            dispatcher.BeginInvoke(() =>
            {
                ContactsList.Clear();
                foreach (var c in e.Results)
                {
                    var con = new ContactEx(c, this.Kindd);
                    if (!string.IsNullOrWhiteSpace(con.DataValue))
                        ContactsList.Add(con);
                }
                evaluating = false;
                RefreshProperty("EmptyList");
                RefreshProperty("ProgressBarVisible");
            });
        }
    }
}
