﻿using Microsoft.Phone.UserData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BartqImHere.Code
{
    public class ContactEx
    {
        private Contact c;
        private Models.ChooseContactsViewModel.Kind kind;

        public ContactPhoneNumber PhoneNo
        {
            get
            {
                return c.PhoneNumbers.FirstOrDefault();
            }
        }

        public ContactEmailAddress Email
        {
            get
            {
                return c.EmailAddresses.FirstOrDefault();
            }
        }

        public string DisplayName
        {
            get
            {
                return c.DisplayName;
            }
        }

        public string DataValue
        {
            get
            {
                if (kind == Models.ChooseContactsViewModel.Kind.Email)
                    return (Email!=null) ? Email.EmailAddress : null;
                else return (PhoneNo!=null)? PhoneNo.PhoneNumber : null;
            }
        }

        public ContactEx(Contact c)
        {
            this.c = c;
        }

        public ContactEx(Contact c, Models.ChooseContactsViewModel.Kind kind) : this(c)
        {
            this.kind = kind;
        }
    }
}
