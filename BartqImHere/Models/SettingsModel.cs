﻿using BartqImHere.Models;
using System;
using System.Collections.Generic;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BartqGoHome.Models
{
    public class SettingsModel
    {
        public const string EMAIL_SUBJECT_TEMPLATE = "I'm over here: #houseno #street #city";
        public const string EMAIL_BODY_TEMPLATE = "Here is my last known position.#nl Latitude: "+
            "#lat #nl Longitude: #lon #nl Timestamp: #timest #nl #nl #houseno #street #nl #city #postalcode #nl #state #country #nl #nl https://maps.google.pl/maps?q=#lat(.),#lon(.)&ll=#lat(.),#lon(.)&z=16";
        public const string TEXT_BODY_TEMPLATE = "#timest Lat:#lat Lon:#lon #nl #houseno #street #nl #city #postalcode #nl #state #country";

        private IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;

        private NotificationSet notifiactions;
        public NotificationSet Notifiactions
        {
            get
            {
                if (notifiactions == null)
                {
                    notifiactions = new NotificationSet(settings, "notify");
                    notifiactions.Load();
                }
                return notifiactions;
            }
        }

        private string geoPermission = null;
        public string GeoPermission
        {
            get
            {
                return GetSetting("GeoPermission", ref geoPermission);
            }
            set
            {
                SaveSetting("GeoPermission", ref geoPermission, value);
            }
        }

        private string emailSubjectTemplate = null;
        public string EmailSubjectTemplate
        {
            get
            {
                var val = GetSetting("EmailSubjectTemplate", ref emailSubjectTemplate);
                if (string.IsNullOrWhiteSpace(val))
                {
                    return EMAIL_SUBJECT_TEMPLATE;
                }
                return val;
            }
            set
            {
                SaveSetting("EmailSubjectTemplate", ref emailSubjectTemplate, value);
            }
        }

        private string emailBodyTemplate = null;
        public string EmailBodyTemplate
        {
            get
            {
                var val = GetSetting("EmailBodyTemplate", ref emailBodyTemplate);
                if (string.IsNullOrWhiteSpace(val))
                {
                    return EMAIL_BODY_TEMPLATE;
                }
                return val;
            }
            set
            {
                SaveSetting("EmailBodyTemplate", ref emailBodyTemplate, value);
            }
        }

        private string textTemplate = null;
        public string TextTemplate
        {
            get
            {
                var val = GetSetting("TextTemplate", ref textTemplate);
                if (string.IsNullOrWhiteSpace(val))
                {
                    return TEXT_BODY_TEMPLATE;
                }
                return val;
            }
            set
            {
                SaveSetting("TextTemplate", ref textTemplate, value);
            }
        }

        private string pitch = null;
        public string Pitch
        {
            get
            {
                var val = GetSetting("Pitch", ref pitch);
                if (string.IsNullOrWhiteSpace(val))
                {
                    return "0";
                }
                return val;
            }
            set
            {
                SaveSetting("Pitch", ref pitch, value);
            }
        }

        private string heading = null;
        public string Heading
        {
            get
            {
                var val = GetSetting("Heading", ref heading);
                if (string.IsNullOrWhiteSpace(val))
                {
                    return "360";
                }
                return val;
            }
            set
            {
                SaveSetting("Heading", ref heading, value);
            }
        }

        private string zoom = null;
        public string Zoom
        {
            get
            {
                var val = GetSetting("Zoom", ref zoom);
                if (string.IsNullOrWhiteSpace(val))
                {
                    return "16";
                }
                return val;
            }
            set
            {
                SaveSetting("Zoom", ref zoom, value);
            }
        }

        private void SaveSetting(string key, ref string @var, string val)
        {
            @var = val;
            settings[key] = @var;
            settings.Save();
        }

        private string GetSetting(string key, ref string @var)
        {
            if (string.IsNullOrWhiteSpace(@var))
                {
                    if (settings.Contains(key))
                        @var = (string)settings[key];
                    else 
                        @var = string.Empty;
                }
            return @var;
        }

    }
}
