﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using BartqImHere.Models;
using Microsoft.Phone.Tasks;
using BartqImHere.Code;
using System.Text;
using System.IO.IsolatedStorage;
using System.Diagnostics;

namespace BartqImHere
{
    public partial class ChooseContacts : PhoneApplicationPage
    {
        public ChooseContacts()
        {
            InitializeComponent();
        }

        protected async override void OnNavigatedTo(NavigationEventArgs e)
        {
            string kindstr = NavigationContext.QueryString["kind"];

            var kind = (ChooseContactsViewModel.Kind)
                Enum.Parse(typeof(ChooseContactsViewModel.Kind), kindstr);

            var vm = new ChooseContactsViewModel(kind, Dispatcher);
            await vm.Fill();
            this.DataContext = vm;

            base.OnNavigatedTo(e);
        }

        private void ApplicationBarIconButton_Click(object sender, EventArgs e)
        {
            var saveContactTask = new SaveContactTask();
            saveContactTask.Completed += saveContactTask_Completed;
            saveContactTask.Show();
        }

        async void saveContactTask_Completed(object sender, SaveContactResult e)
        {
            var vm = (ChooseContactsViewModel)this.DataContext;
            await vm.Fill();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            MoveTempFiles();

            App.Settings.Notifiactions.Add(DateTime.Now);

            MyLiveTile.UpdateLiveTile("tile");

            var vm = (ChooseContactsViewModel)this.DataContext;
            if (vm.Kindd == ChooseContactsViewModel.Kind.Email)
                SendEmail();
            else
                SendText();
        }

        public static void MoveTempFiles()
        {
            using (IsolatedStorageFile isf = IsolatedStorageFile.GetUserStoreForApplication())
            {
                try
                {
                    if (isf.FileExists("/Shared/ShellContent/tile.jpg"))
                        isf.DeleteFile("/Shared/ShellContent/tile.jpg");
                    if(isf.FileExists("/temp.jpg"))
                        isf.MoveFile("/temp.jpg", "/Shared/ShellContent/tile.jpg");
                }
                catch {
                    if (Debugger.IsAttached)
                    {
                        Debugger.Break();
                    }

                }
                try
                {
                    if (isf.FileExists("/Shared/ShellContent/widetile.jpg"))
                        isf.DeleteFile("/Shared/ShellContent/widetile.jpg");
                    if (isf.FileExists("/widetemp.jpg"))
                        isf.MoveFile("/widetemp.jpg", "/Shared/ShellContent/widetile.jpg");
                }
                catch {
                    if (Debugger.IsAttached)
                    {
                        Debugger.Break();
                    }

                }
            }
        }

        private void SendText()
        {
            string body = App.FormatMessage(App.Settings.TextTemplate);

            SmsComposeTask sms = new SmsComposeTask();
            sms.Body = body;
            sms.To = GetRecipients();
            sms.Show();
        }

        private void SendEmail()
        {
            var body = App.FormatMessage(App.Settings.EmailBodyTemplate);

            string subject = App.FormatMessage(App.Settings.EmailSubjectTemplate,false);

            EmailComposeTask emailComposeTask = new EmailComposeTask();

            emailComposeTask.Subject = subject;
            emailComposeTask.Body = body;
            emailComposeTask.To = GetRecipients();

            emailComposeTask.Show();
        }

        private string GetRecipients()
        {
            var items = new StringBuilder();
            foreach (ContactEx contact in ContactsList.SelectedItems)
            {
                if (items.Length > 0) items.Append(";");
                items.Append(contact.DataValue);
            }
            return items.ToString();
        }

        private void ContactsList_IsSelectionEnabledChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if(ContactsList!=null)
                ContactsList.IsSelectionEnabled = true;
        }

        private void ApplicationBarIconButton_Click_1(object sender, EventArgs e)
        {
            saveContactTask_Completed(null, null);
        }
    }
}