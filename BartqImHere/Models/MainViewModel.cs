﻿using BartqGoHome.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace BartqImHere.Models
{
    public class MainViewModel : BaseViewModel
    {
        private string address;
        public string Address
        {
            get
            {
                return address;
            }
            set
            {
                if (SetField(ref address, value, "Address"))
                    OnPropertyChanged("AddressPanelVisible");
            }
        }

        public bool PermissionChecked { get; set; }

        public Visibility AddressPanelVisible
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(Address))
                    return Visibility.Visible;
                else return Visibility.Collapsed;
            }
        }

        private double? pitch;
        public double Pitch
        {
            get
            {
                if (pitch == null)
                    pitch = double.Parse(App.Settings.Pitch);
                return pitch.Value;
            }
            set
            {

                SetField(ref pitch, (v) => { App.Settings.Pitch = value.ToString(); }, value, "Pitch");
            }
        }

        private double? heading;
        public double Heading
        {
            get
            {
                if (heading == null)
                    heading = double.Parse(App.Settings.Heading);
                return heading.Value;
            }
            set
            {

                SetField(ref heading, (v) => { App.Settings.Heading = value.ToString(); }, value, "Heading");
            }
        }

        private double? zoom;
        public double Zoom
        {
            get
            {
                if (zoom == null)
                    zoom = double.Parse(App.Settings.Zoom);
                return zoom.Value;
            }
            set
            {

                SetField(ref zoom, (v) => { App.Settings.Zoom = value.ToString(); }, value, "Zoom");
            }
        }
    }
}
