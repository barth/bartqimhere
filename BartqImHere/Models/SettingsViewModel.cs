﻿using BartqGoHome.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BartqImHere.Models
{
    public class SettingsViewModel : BaseViewModel
    {
        private bool? geoPermission;
        public bool GeoPermission
        {
            get
            {
                if (geoPermission == null)
                    geoPermission = App.Settings.GeoPermission == "yes";
                return geoPermission.Value;
            }
            set
            {
                string val = "yes";
                if (!value)
                    val = "no";
                SetField(ref geoPermission, (v) => { App.Settings.GeoPermission = val; }, value, "GeoPermission");
            }
        }

        private string emailsubject;
        public string EmailSubject
        {
            get
            {
                if (emailsubject == null)
                {
                    emailsubject = App.Settings.EmailSubjectTemplate;
                }
                return emailsubject;
            }
            set
            {

                SetField(ref emailsubject, (v) => { App.Settings.EmailSubjectTemplate = value; }, value, "EmailSubject");
            }
        }

        private string emailbody;
        public string EmailBody
        {
            get
            {
                if (emailbody == null)
                    emailbody = App.Settings.EmailBodyTemplate;
                return emailbody;
            }
            set
            {

                SetField(ref emailbody, (v) => { App.Settings.EmailBodyTemplate = value; }, value, "EmailBody");
            }
        }

        private string textbody;
        public string TextBody
        {
            get
            {
                if (textbody == null)
                    textbody = App.Settings.TextTemplate;
                return textbody;
            }
            set
            {

                SetField(ref textbody, (v) => { App.Settings.TextTemplate = value; }, value, "TextBody");
            }
        }
    }
}
