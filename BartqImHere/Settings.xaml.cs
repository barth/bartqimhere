﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using BartqImHere.Models;
using BartqImHere.Code;
using BartqGoHome.Models;
using Coding4Fun.Toolkit.Controls;

namespace BartqImHere
{
    public partial class Settings : PhoneApplicationPage
    {
        public Settings()
        {
            InitializeComponent();

            DataContext = new SettingsViewModel();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            App.Settings.Notifiactions.RemoveAll();
            MyLiveTile.UpdateLiveTile("tile");
        }

        private void RoundButton_Click(object sender, RoutedEventArgs e)
        {
            var vm = (SettingsViewModel)DataContext;
            vm.EmailSubject = SettingsModel.EMAIL_SUBJECT_TEMPLATE;
        }

        private void RoundButton_Click_1(object sender, RoutedEventArgs e)
        {
            var vm = (SettingsViewModel)DataContext;
            vm.EmailBody = SettingsModel.EMAIL_BODY_TEMPLATE;
        }

        private void RoundButton_Click_2(object sender, RoutedEventArgs e)
        {
            var vm = (SettingsViewModel)DataContext;
            vm.TextBody = SettingsModel.TEXT_BODY_TEMPLATE;
        }

        private void Ins(TextBox target,string text)
        {
            int val = target.SelectionStart;
            target.Text = target.Text.Insert(val, text);
            target.Focus();
            target.SelectionStart = val + text.Length;
        }

        private void RoundButton_Click_3(object sender, RoutedEventArgs e)
        {
            ShowMessagePrompt(emailsubjecttemplate);
        }

        private void ShowMessagePrompt(TextBox target)
        {
            var messagePrompt = new MessagePrompt
            {
                Title = "UI glich :/",
                Message = "Please tap me a little longer...",
                IsAppBarVisible = false,
                IsCancelVisible = false
            };
            messagePrompt.Completed += (_,__) =>
            {
                target.Focus();
            };
            messagePrompt.Show();
        }

        private void RoundButton1_Click_3(object sender, RoutedEventArgs e)
        {
            ShowMessagePrompt(emailbodytemplate);
        }

        private void RoundButton2_Click_3(object sender, RoutedEventArgs e)
        {
            ShowMessagePrompt(textsubjecttemplate);
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            Ins(emailsubjecttemplate, "#nl ");
        }

        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            Ins(emailsubjecttemplate, "#timest ");
        }

        private void MenuItem_Click_2(object sender, RoutedEventArgs e)
        {
            Ins(emailsubjecttemplate, "#lat ");
        }

        private void MenuItem_Click_3(object sender, RoutedEventArgs e)
        {
            Ins(emailsubjecttemplate, "#lon ");
        }

        private void MenuItem_Click_2b(object sender, RoutedEventArgs e)
        {
            Ins(emailsubjecttemplate, "#lat(.) ");
        }

        private void MenuItem_Click_3b(object sender, RoutedEventArgs e)
        {
            Ins(emailsubjecttemplate, "#lon(.) ");
        }

        private void MenuItem1_Click_2b(object sender, RoutedEventArgs e)
        {
            Ins(emailsubjecttemplate, "#lat(.) ");
        }

        private void MenuItem1_Click_3b(object sender, RoutedEventArgs e)
        {
            Ins(emailsubjecttemplate, "#lon(.) ");
        }

        private void MenuItem2_Click_2b(object sender, RoutedEventArgs e)
        {
            Ins(emailsubjecttemplate, "#lat(.) ");
        }

        private void MenuItem2_Click_3b(object sender, RoutedEventArgs e)
        {
            Ins(emailsubjecttemplate, "#lon(.) ");
        }

        private void MenuItem_Click_4(object sender, RoutedEventArgs e)
        {
            Ins(emailsubjecttemplate, "#houseno ");
        }

        private void MenuItem_Click_5(object sender, RoutedEventArgs e)
        {
            Ins(emailsubjecttemplate, "#street ");
        }

        private void MenuItem_Click_6(object sender, RoutedEventArgs e)
        {
            Ins(emailsubjecttemplate, "#city ");
        }

        private void MenuItem_Click_7(object sender, RoutedEventArgs e)
        {
            Ins(emailsubjecttemplate, "#postalcode ");
        }

        private void MenuItem_Click_8(object sender, RoutedEventArgs e)
        {
            Ins(emailsubjecttemplate, "#state ");
        }

        private void MenuItem_Click_9(object sender, RoutedEventArgs e)
        {
            Ins(emailsubjecttemplate, "#country ");
        }

        private void MenuItem1_Click(object sender, RoutedEventArgs e)
        {
            Ins(emailbodytemplate, "#nl ");
        }

        private void MenuItem1_Click_1(object sender, RoutedEventArgs e)
        {
            Ins(emailbodytemplate, "#timest ");
        }

        private void MenuItem1_Click_2(object sender, RoutedEventArgs e)
        {
            Ins(emailbodytemplate, "#lat ");
        }

        private void MenuItem1_Click_3(object sender, RoutedEventArgs e)
        {
            Ins(emailbodytemplate, "#lon ");
        }

        private void MenuItem1_Click_4(object sender, RoutedEventArgs e)
        {
            Ins(emailbodytemplate, "#houseno ");
        }

        private void MenuItem1_Click_5(object sender, RoutedEventArgs e)
        {
            Ins(emailbodytemplate, "#street ");
        }

        private void MenuItem1_Click_6(object sender, RoutedEventArgs e)
        {
            Ins(emailbodytemplate, "#city ");
        }

        private void MenuItem1_Click_7(object sender, RoutedEventArgs e)
        {
            Ins(emailbodytemplate, "#postalcode ");
        }

        private void MenuItem1_Click_8(object sender, RoutedEventArgs e)
        {
            Ins(emailbodytemplate, "#state ");
        }

        private void MenuItem1_Click_9(object sender, RoutedEventArgs e)
        {
            Ins(emailbodytemplate, "#country ");
        }

        private void MenuItem2_Click(object sender, RoutedEventArgs e)
        {
            Ins(textsubjecttemplate, "#nl ");
        }

        private void MenuItem2_Click_1(object sender, RoutedEventArgs e)
        {
            Ins(textsubjecttemplate, "#timest ");
        }

        private void MenuItem2_Click_2(object sender, RoutedEventArgs e)
        {
            Ins(textsubjecttemplate, "#lat ");
        }

        private void MenuItem2_Click_3(object sender, RoutedEventArgs e)
        {
            Ins(textsubjecttemplate, "#lon ");
        }

        private void MenuItem2_Click_4(object sender, RoutedEventArgs e)
        {
            Ins(textsubjecttemplate, "#houseno ");
        }

        private void MenuItem2_Click_5(object sender, RoutedEventArgs e)
        {
            Ins(textsubjecttemplate, "#street ");
        }

        private void MenuItem2_Click_6(object sender, RoutedEventArgs e)
        {
            Ins(textsubjecttemplate, "#city ");
        }

        private void MenuItem2_Click_7(object sender, RoutedEventArgs e)
        {
            Ins(textsubjecttemplate, "#postalcode ");
        }

        private void MenuItem2_Click_8(object sender, RoutedEventArgs e)
        {
            Ins(textsubjecttemplate, "#state ");
        }

        private void MenuItem2_Click_9(object sender, RoutedEventArgs e)
        {
            Ins(textsubjecttemplate, "#country ");
        }

    }
}