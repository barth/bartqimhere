﻿using System;
using System.Collections.Generic;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BartqImHere.Models
{
    public class NotificationSet : HashSet<DateTime>
    {
        IsolatedStorageSettings settings;
        string prefix;

        public NotificationSet(IsolatedStorageSettings source, string prefix)
        {
            this.settings = source;
            this.prefix = prefix;
        }

        public void Load()
        {
            Clear();
            var setcopy = settings.ToList();
            foreach (var item in setcopy)
            {
                if (item.Key.StartsWith(prefix))
                    this.Add(DateTime.Parse(item.Value.ToString()));
            }
        }

        public new bool Add(DateTime key)
        {
            var res = false;
            if ((DateTime.Now - key).TotalHours < 24.0)
            {
                res = base.Add(key);
                settings[prefix + this.Count.ToString()] = key.ToString();
            }
            RemoveAllOld();
            return res;
        }

        public new bool Remove(DateTime key)
        {
            var keystr = key.ToString();
            var keyset = settings.Where(kvp => kvp.Value.ToString() == keystr).Select(kvp => kvp.Key).FirstOrDefault();
            if(keyset!=null)
                settings.Remove(keyset);
            return base.Remove(key);
        }

        public void RemoveAllOld()
        {
            var setcopy = this.ToList();
            DateTime now = DateTime.Now;
            foreach (var key in setcopy)
            {
                if ((now - key).TotalHours >= 24.0)
                    Remove(key);
            }
        }

        public void RemoveAll()
        {
            var setcopy = this.ToList();
            foreach (var key in setcopy)
            {
                Remove(key);
            }
        }
    }
}
